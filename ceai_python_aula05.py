""" Este módulo deve ser utilizado com a aula 5 do curso de python """
import functools
from inspect import signature
from inspect import getsource
import operator
import copy
import traceback
import html
import itertools


class SolucaoResultado():
    def __init__(self, res=True, ex=None):
        self._res = res
        self._ex = ex
    
    def __str__(self):
        if self._res:
            return "Resultado Correto"
        if self._ex is not None:
            return "Resultado Incorreto: " + str(ex)
        return "Resultado Incorreto"

    def __bool__(self):
        return self._res
        
    def _repr_html_(self):
        if self._res:
            return '<div style="border-style:solid;border-color:green">Exercício Correto!</div>'
        else:
            if self._ex:
                if self._ex.__cause__:
                    cause = ''.join(traceback.format_exception(type(self._ex.__cause__),self._ex.__cause__,self._ex.__cause__.__traceback__))
                    msg = '<div style="border-style:solid;border-color:red">Exercício Incorreto!<br/>{0}<br/>Exceção Causadora:<br/><pre>{1}</pre></div>'.format(html.escape(self._ex.formatted_message()),html.escape(cause))
                else:
                    msg = '<div style="border-style:solid;border-color:red">Exercício Incorreto!<br/>{0}</div>'.format(html.escape(self._ex.formatted_message()))
            else:
                msg = "Exercício Incorreto"
        return msg
    
class SolucaoInvalida(BaseException):
    def __init__(self, *args):
        if args:
            self._message = args[0]
        else:
            self._message = None
            
    def formatted_message(self):
        return self._message

    def __str__(self):
        print('calling str')
        if self._message:
            return 'SolucaoInvalida, {0} '.format(self._message)
        else:
            return 'SolucaoInvalida'



def VerificaAmbiente():
    print("Ambiente inicializado com sucesso")
    return True



def valida_ex_01_01(func):
    def testa(arg):
        try:
            ret = func(arg)
        except Exception as ex:
            raise SolucaoInvalida("A função lançou uma exceção quando recebeu o parâmetro " + str(arg)) from ex
        if ret is  None:
            raise SolucaoInvalida("A função não retornou nenhum valor")
        return ret
    try:    
        if type(func)!=type(valida_ex_01_01):
            raise SolucaoInvalida("Você deve passar uma função")
        if len(signature(func).parameters)!=1:
            raise SolucaoInvalida("A função deve aceitar exatamente um parâmetro")
        listas = [[1,2,3,4], ['a', 'b', 'c'], [0.1, 0.0, 0.2]]
        valores = [1, 'b', 0.0]
        for valor in valores:
            f = func(valor)
            if type(f)!=type(valida_ex_01_01):
                raise SolucaoInvalida("A função não retornou uma nova função quando recebeu o parâmetro " + str(valor))
            if len(signature(f).parameters)!=1:
                raise SolucaoInvalida("A nova função retornada deve aceitar exatamente um parâmetro")
            for lista in listas:
                try:
                    ret = f(lista)
                except Exception as ex:
                    raise SolucaoInvalida("A nova função, gerada com o argumento " + str(valor) + " lançou uma exceção quando recebeu o parâmetro " + str(lista)) from ex
                if ret is None:
                    raise SolucaoInvalida("A nova função, gerada com o argumento " + str(valor) + " não retornou nenhum valor quando recebeu o parâmetro " + str(lista))
                if type(ret) != bool:
                    raise SolucaoInvalida("A nova função, gerada com o argumento " + str(valor) + " não retornou um booleano valor quando recebeu o parâmetro " + str(lista))
                gab = valor in lista
                if ret != gab:
                    raise SolucaoInvalida("A nova função, gerada com o argumento " + str(valor) + " retornou " + str(ret) + " quando recebeu o parâmetro " + str(lista) + ". O valor esperado era " + str(gab))
    except SolucaoInvalida as ex:
        return SolucaoResultado(False, ex)
    return SolucaoResultado()

def valida_ex_01_02_01(func):
    def testa(arg1, arg2):
        try:
            ret = func(arg1, arg2)
        except Exception as ex:
            raise SolucaoInvalida("A função lançou uma exceção quando recebeu o parâmetros " + str(arg1) + " e " + str(arg2)) from ex
        if ret is  None:
            raise SolucaoInvalida("A função não retornou nenhum valor")
        return ret
    try:    
        if type(func)!=type(valida_ex_01_02_01):
            raise SolucaoInvalida("Você deve passar uma função")
        if func.__name__!='<lambda>':
            raise SolucaoInvalida("Sua função deve ser declarada por uma expressão lambda")
        if len(signature(func).parameters)!=2:
            raise SolucaoInvalida("A função deve aceitar exatamente dois parâmetros")
        vx = [-3, -2, -1, 0, 1, 2 ,3]
        for v1, v2 in itertools.product(vx, vx):
            ret = testa(v1, v2)
            gab = v1 if v1 > v2 else v2
            if ret != gab:
                raise SolucaoInvalida("A função retornou " + str(ret) + " quando recebeu os parâmetros " + str(v1) + ", " + str(v2) + ". O valor esperado era " + str(gab))
    except SolucaoInvalida as ex:
        return SolucaoResultado(False, ex)
    return SolucaoResultado()
    
def valida_ex_02_01_01(func):
    def testa(arg1, arg2):
        try:
            ret = func(arg1, arg2)
        except Exception as ex:
            raise SolucaoInvalida("A função lançou uma exceção quando recebeu os parâmetros " + str(arg)) from ex
        if ret is  None:
            raise SolucaoInvalida("A função não retornou nenhum valor")
        if type(ret)!=list:
            raise SolucaoInvalida("A função não retornou uma sequência")
        return ret
    try:    
        if type(func)!=type(valida_ex_01_02_01):
            raise SolucaoInvalida("Você deve passar uma função")
        if len(signature(func).parameters)!=2:
            raise SolucaoInvalida("A função deve aceitar exatamente dois parâmetros")
        try:
            source = getsource(func)
        except TypeError:
            raise SolucaoInvalida("A sua função deve ser composta da declaração e apenas uma linha de código")
        lines = source.split('\n')
        lcount = 0
        for l in lines:
            ll = l.lstrip()
            if len(ll)==0 or ll.startswith('#') or ll.startswith('""""'):
                continue
            lcount += 1 
        if lcount > 2:
            raise SolucaoInvalida("A sua função deve ser composta da declaração e apenas uma linha de código"+str(lcount))
        vx = [(0,0), (3,2), (5, 2)]
        for n, x in vx:
            ret = testa(n, x)
            gab = [i*x for i in range(n)]
            if ret != gab:
                raise SolucaoInvalida("A função retornou " + str(ret) + " quando recebeu os parâmetros " + str(n) + ", " + str(x) + ". O valor esperado era " + str(gab))
    except SolucaoInvalida as ex:
        return SolucaoResultado(False, ex)
    return SolucaoResultado()


if __name__ != 'main':
    VerificaAmbiente()
