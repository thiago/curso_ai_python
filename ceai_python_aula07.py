""" Este módulo deve ser utilizado com a aula 6 do curso de python """
import functools
from inspect import signature
from inspect import getsource
import operator
import copy
import traceback
import html
import itertools
import sys
import pathlib
import os
import re
import json
import datetime



class SolucaoResultado():
    def __init__(self, res=True, ex=None):
        self._res = res
        self._ex = ex
    
    def __str__(self):
        if self._res:
            return "Resultado Correto"
        if self._ex is not None:
            return "Resultado Incorreto: " + str(ex)
        return "Resultado Incorreto"

    def __bool__(self):
        return self._res
        
    def _repr_html_(self):
        if self._res:
            return '<div style="border-style:solid;border-color:green">Exercício Correto!</div>'
        else:
            if self._ex:
                if self._ex.__cause__:
                    cause = ''.join(traceback.format_exception(type(self._ex.__cause__),self._ex.__cause__,self._ex.__cause__.__traceback__))
                    msg = '<div style="border-style:solid;border-color:red">Exercício Incorreto!<br/>{0}<br/>Exceção Causadora:<br/><pre>{1}</pre></div>'.format(html.escape(self._ex.formatted_message()),html.escape(cause))
                else:
                    msg = '<div style="border-style:solid;border-color:red">Exercício Incorreto!<br/>{0}</div>'.format(html.escape(self._ex.formatted_message()))
            else:
                msg = "Exercício Incorreto"
        return msg
    
class SolucaoInvalida(BaseException):
    def __init__(self, *args):
        if args:
            self._message = args[0]
        else:
            self._message = None
            
    def formatted_message(self):
        return self._message

    def __str__(self):
        print('calling str')
        if self._message:
            return 'SolucaoInvalida, {0} '.format(self._message)
        else:
            return 'SolucaoInvalida'



def VerificaAmbiente():
    print("Ambiente inicializado com sucesso")
    return True



def valida_ex_01_01_01():
    try: 
        if 'test01' not in sys.modules:
            raise SolucaoInvalida("O módulo test01 não foi importado")
    except SolucaoInvalida as ex:
        return SolucaoResultado(False, ex)
    return SolucaoResultado()
    
def valida_ex_01_01_02():
    try: 
        if 'test01' not in sys.modules:
            raise SolucaoInvalida("O módulo test01 não foi importado")
        if not sys.modules['test01'].test_it():
            raise SolucaoInvalida("A função minha_funcao() não foi chamada")
    except SolucaoInvalida as ex:
        return SolucaoResultado(False, ex)
    return SolucaoResultado()

def valida_ex_01_02_01(func):
    try:    
        if not pathlib.Path('curso_ai_python').is_dir():
            raise SolucaoInvalida("O diretório não foi criado!")
    except SolucaoInvalida as ex:
        return SolucaoResultado(False, ex)
    return SolucaoResultado()
    
def valida_ex_05_01_01(linhas):
    try:    
        if type(linhas)!=list:
            raise SolucaoInvalida("Você deve passar uma sequência")
            
        arquivo = open("curso_ai_python/dados/dados01.txt")
        linhas_gab = arquivo.readlines()
        if len(linhas)!=len(linhas_gab):
            raise SolucaoInvalida("Você passou um número incorreto de linhas")
        for i in range(len(linhas)):
            if linhas[i]!=linhas_gab[i]:
                raise SolucaoInvalida("A linha " + str(i) + " contém " + linhas[i] + " mas deveria conter " + linhas_gab[i])
    except SolucaoInvalida as ex:
        return SolucaoResultado(False, ex)
    return SolucaoResultado()
    
def valida_ex_05_01_02(linha):
    try:    
        if type(linha)!=str:
            raise SolucaoInvalida("Você deve passar uma string")
        if linha != 'parametro_01 = 2\n':
            raise SolucaoInvalida("Conteúdo errado, você passou " + str(linha))
    except SolucaoInvalida as ex:
        return SolucaoResultado(False, ex)
    return SolucaoResultado()
    
def valida_ex_05_01_03():
    try:    
        if not pathlib.Path('/content/resultado.txt').is_file():
            raise SolucaoInvalida("O arquivo não existe")
        file = open('/content/resultado.txt', 'rt')
        linhas = file.readlines()
        if len(linhas)!=10:
            raise SolucaoInvalida("O arquivo tem "+str(len(linhas))+ " e deveria ter 10 linhas.")
        for i in range(10):
            try:
                if int(linhas[i])!=((i+1)*(i+1)):
                    raise SolucaoInvalida("A linha " + str(i+1) + " contém " + linhas[i] + " e deveria conter " + str((i+1)*(i+1)))
            except ValueError:
                raise SolucaoInvalida("A linha " + str(i+1) + " contém " + linhas[i] + " e deveria conter " + str((i+1)*(i+1)))
    except SolucaoInvalida as ex:
        return SolucaoResultado(False, ex)
    return SolucaoResultado()
    
def valida_ex_05_02_01():
    try:    
        if not pathlib.Path('/content/resultado.json').is_file():
            raise SolucaoInvalida("O arquivo não existe")
        file = open('/content/resultado.json', 'rt')
        conteudo = file.read()
        if len(conteudo)==0:
            raise SolucaoInvalida("O arquivo está vazio.")
        try:
            obj = json.loads(conteudo)
        except json.JSONDecodeError as err:
            raise SolucaoInvalida("Não foi possível decodificar o conteúdo JSON do arquivo.") from err
        if type(obj)!= dict:
            raise SolucaoInvalida("O objeto armazenado não é um dicionário.") from err
        chaves = []
        for chave in obj:
            try:
                i = int(chave)
            except ValueError as err:
                raise SolucaoInvalida("A chave " + chave + " não é um inteiro válido!") from err
            if i<1 or i > 100:
                raise SolucaoInvalida("A chave " + str(i) + " está fora do alcance especificado!")
            valor = obj[chave]
            if type(valor)!=int:
                raise SolucaoInvalida("O valor " + str(valor) + " não é um inteiro!")
            if valor != i*i:
                raise SolucaoInvalida("O valor " + str(valor) + " armazenado sob a chave " + chave + "está incorreto!")
            chaves.append(i)
        for i in range(1, 101):
            if i not in chaves:
                raise SolucaoInvalida("O dicionário não contém a chave " + str(i))
        
    except SolucaoInvalida as ex:
        return SolucaoResultado(False, ex)
    return SolucaoResultado()
    
def valida_ex_05_03_01(func):
    def testa():
        try:
            ret = func()
        except Exception as ex:
            raise SolucaoInvalida("A função lançou uma exceção") from ex
        if ret is  None:
            raise SolucaoInvalida("A função não retornou nenhum valor")
        if type(ret)!=str:
            raise SolucaoInvalida("A função não retornou uma string")
        return ret
    try:    
        if type(func)!=type(valida_ex_01_02_01):
            raise SolucaoInvalida("Você deve passar uma função")
        if len(signature(func).parameters)!=0:
            raise SolucaoInvalida("A função não deve aceitar parâmetros")
        strdata_1 = testa()
        agora_1 = datetime.datetime.utcnow()
        try:
            data_1 = datetime.datetime.strptime(strdata_1, "%Y-%m-%dT%H:%M:%S.%f-03:00") + datetime.timedelta(seconds=(3*60*60))
        except ValueError as err:
            raise SolucaoInvalida("A string não representa uma data válida") from err
        strdata_2 = testa()
        agora_2 = datetime.datetime.utcnow()
        try:
            data_2 = datetime.datetime.strptime(strdata_2, "%Y-%m-%dT%H:%M:%S.%f-03:00") + datetime.timedelta(seconds=(3*60*60))
        except ValueError as err:
            raise SolucaoInvalida("A string não representa uma data válida") from err
        if abs((data_1 - agora_1).total_seconds()) > 60:
            raise SolucaoInvalida("A hora " + str(data_1) + " não está correta")
        if abs((data_2 - agora_2).total_seconds()) > 60:
            raise SolucaoInvalida("A hora " + str(data_2) + " não está correta")
        if (data_2-data_1).total_seconds() < 0:
            raise SolucaoInvalida("A data " + str(data_2) + " foi retornada depois da " + str(data_2) )
    except SolucaoInvalida as ex:
        return SolucaoResultado(False, ex)
    return SolucaoResultado()


if __name__ != 'main':
    VerificaAmbiente()
