""" Este módulo deve ser utilizado com a aula 6 do curso de python """
import functools
from inspect import signature
from inspect import getsource
import operator
import copy
import traceback
import html
import itertools
import sys
import pathlib
import os
import re


class SolucaoResultado():
    def __init__(self, res=True, ex=None):
        self._res = res
        self._ex = ex
    
    def __str__(self):
        if self._res:
            return "Resultado Correto"
        if self._ex is not None:
            return "Resultado Incorreto: " + str(ex)
        return "Resultado Incorreto"

    def __bool__(self):
        return self._res
        
    def _repr_html_(self):
        if self._res:
            return '<div style="border-style:solid;border-color:green">Exercício Correto!</div>'
        else:
            if self._ex:
                if self._ex.__cause__:
                    cause = ''.join(traceback.format_exception(type(self._ex.__cause__),self._ex.__cause__,self._ex.__cause__.__traceback__))
                    msg = '<div style="border-style:solid;border-color:red">Exercício Incorreto!<br/>{0}<br/>Exceção Causadora:<br/><pre>{1}</pre></div>'.format(html.escape(self._ex.formatted_message()),html.escape(cause))
                else:
                    msg = '<div style="border-style:solid;border-color:red">Exercício Incorreto!<br/>{0}</div>'.format(html.escape(self._ex.formatted_message()))
            else:
                msg = "Exercício Incorreto"
        return msg
    
class SolucaoInvalida(BaseException):
    def __init__(self, *args):
        if args:
            self._message = args[0]
        else:
            self._message = None
            
    def formatted_message(self):
        return self._message

    def __str__(self):
        print('calling str')
        if self._message:
            return 'SolucaoInvalida, {0} '.format(self._message)
        else:
            return 'SolucaoInvalida'



def VerificaAmbiente():
    print("Ambiente inicializado com sucesso")
    return True



def valida_ex_01_01_01():
    try: 
        if 'test01' not in sys.modules:
            raise SolucaoInvalida("O módulo test01 não foi importado")
    except SolucaoInvalida as ex:
        return SolucaoResultado(False, ex)
    return SolucaoResultado()
    
def valida_ex_01_01_02():
    try: 
        if 'test01' not in sys.modules:
            raise SolucaoInvalida("O módulo test01 não foi importado")
        if not sys.modules['test01'].test_it():
            raise SolucaoInvalida("A função minha_funcao() não foi chamada")
    except SolucaoInvalida as ex:
        return SolucaoResultado(False, ex)
    return SolucaoResultado()

def valida_ex_01_02_01(func):
    try:    
        if not pathlib.Path('/content/gdrive/My Drive/cursoai_exercicios').is_dir():
            raise SolucaoInvalida("O diretório não foi criado!")
    except SolucaoInvalida as ex:
        return SolucaoResultado(False, ex)
    return SolucaoResultado()



def valida_ex_03_01_01():
    try:
        if 'pacote_aula06.teste' not in sys.modules:
            raise SolucaoInvalida("O módulo teste do pacote pacote_aula06 não foi importado")
    except SolucaoInvalida as ex:
        return SolucaoResultado(False, ex)
    return SolucaoResultado()

def valida_ex_03_01_02():
    try:
        if 'pacote_aula06.teste' not in sys.modules:
            raise SolucaoInvalida("O módulo teste do pacote pacote_aula06 não foi importado")
        if not sys.modules['pacote_aula06.teste']._test_it():
            raise SolucaoInvalida("A função outra_funcao() não foi chamada")
    except SolucaoInvalida as ex:
        return SolucaoResultado(False, ex)
    return SolucaoResultado()


    
def valida_ex_05_01_01(linhas):
    try:    
        if type(linhas)!=list:
            raise SolucaoInvalida("Você deve passar uma sequência")
            
        arquivo = open("/content/gdrive/My Drive/cursoai_python_aula_03/dados/dados01.txt")
        linhas_gab = arquivo.readlines()
        if len(linhas)!=len(linhas_gab):
            raise SolucaoInvalida("Você passou um número incorreto de linhas")
        for i in range(len(linhas)):
            if linhas[i]!=linhas_gab[i]:
                raise SolucaoInvalida("A linha " + str(i) + " contém " + linhas[i] + " mas deveria conter " + linhas_gab[i])
    except SolucaoInvalida as ex:
        return SolucaoResultado(False, ex)
    return SolucaoResultado()
    
def valida_ex_05_01_02(linha):
    try:    
        if type(linha)!=str:
            raise SolucaoInvalida("Você deve passar uma string")
        if linha != 'parametro_01 = 2\n':
            raise SolucaoInvalida("Conteúdo errado, você passou " + str(linha))
    except SolucaoInvalida as ex:
        return SolucaoResultado(False, ex)
    return SolucaoResultado()
    
def valida_ex_05_03_01():
    try:    
        if not pathlib.Path('/content/resultado.txt').is_file():
            raise SolucaoInvalida("O arquivo não existe")
        file = open('/content/resultado.txt', 'rt')
        linhas = file.readlines()
        if len(linhas)!=10:
            raise SolucaoInvalida("O arquivo tem "+str(len(linhas))+ " e deveria ter 10 linhas.")
        for i in range(10):
            try:
                if int(linhas[i])!=((i+1)*(i+1)):
                    raise SolucaoInvalida("A linha " + str(i+1) + " contém " + linhas[i] + " e deveria conter " + str((i+1)*(i+1)))
            except ValueError:
                raise SolucaoInvalida("A linha " + str(i+1) + " contém " + linhas[i] + " e deveria conter " + str((i+1)*(i+1)))
    except SolucaoInvalida as ex:
        return SolucaoResultado(False, ex)
    return SolucaoResultado()


if __name__ != 'main':
    VerificaAmbiente()
