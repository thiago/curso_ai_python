""" Este módulo deve ser utilizado com a aula 4 do curso de python """
import functools
from inspect import signature, isfunction, ismethod
import operator
import copy
import traceback
import html

class ClasseExemplo_01:
  def atribui_valor(self, x):
    self._valor = x
 
  def recupera_valor(self):
    return self._valor
    

   
class SolucaoResultado():
    def __init__(self, res=True, ex=None):
        self._res = res
        self._ex = ex
    
    def __str__(self):
        if self._res:
            return "Resultado Correto"
        if self._ex is not None:
            return "Resultado Incorreto: " + str(ex)
        return "Resultado Incorreto"

    def __bool__(self):
        return self._res
        
    def _repr_html_(self):
        if self._res:
            return '<div style="border-style:solid;border-color:green">Exercício Correto!</div>'
        else:
            if self._ex:
                if self._ex.__cause__:
                    cause = ''.join(traceback.format_exception(type(self._ex.__cause__),self._ex.__cause__,self._ex.__cause__.__traceback__))
                    msg = '<div style="border-style:solid;border-color:red">Exercício Incorreto!<br/>{0}<br/>Exceção Causadora:<br/><pre>{1}</pre></div>'.format(html.escape(self._ex.formatted_message()),html.escape(cause))
                else:
                    msg = '<div style="border-style:solid;border-color:red">Exercício Incorreto!<br/>{0}</div>'.format(html.escape(self._ex.formatted_message()))
            else:
                msg = "Exercício Incorreto"
        return msg
    
class SolucaoInvalida(BaseException):
    def __init__(self, *args):
        if args:
            self._message = args[0]
        else:
            self._message = None
            
    def formatted_message(self):
        return self._message

    def __str__(self):
        print('calling str')
        if self._message:
            return 'SolucaoInvalida, {0} '.format(self._message)
        else:
            return 'SolucaoInvalida'



def VerificaAmbiente():
    print("Ambiente inicializado com sucesso")
    return True


def valida_ex_01_01(a):
    try:
        if a is None:
            raise SolucaoInvalida("O parâmetro passado é None.")
        if type(a)!=ClasseExemplo_01:
            raise SolucaoInvalida("O tipo de dado esperado é um objeto do tipo ClasseExemplo_01. O passado foi: " + str(type(a)))
        atributos_a = dir(a)
        atributos_b = dir(ClasseExemplo_01())
        for attr in atributos_a:
            if attr not in atributos_b:
                raise SolucaoInvalida("O objeto a possui um atributo inesperado: " + str(attr))
    except SolucaoInvalida as ex:
        return SolucaoResultado(False, ex)
    return SolucaoResultado()


class __NoWrite_ClasseExemplo_01(ClasseExemplo_01):
    def __init__(self, valor):
        self.__dict__['_valor'] = valor
    
    def atribui_valor(self, x):
        self.__dict__['_valor'] = x
        
    def recupera_valor(self):
        return self.__dict__['_valor']

    def __setattribute__(self, atr, val):
        raise Exception("O atributo " + str(atr) + "não pode ser modificado diretamente")
        
    def __getattribute__(self, atr):
        if atr=='_valor':
            raise Exception("O atributo _valor não pode ser acessado diretamente")
        return object.__getattribute__(self, atr)
        
    def __str__(self):
        return str(self.__dict__['_valor'])

def valida_ex_02_01(func):
    def testa(arg):
        try:
            ret = func(arg)
        except Exception as ex:
            raise SolucaoInvalida("A função lançou uma exceção quando recebeu o parâmetro " + str(arg)) from ex
        if ret is not None:
            raise SolucaoInvalida("A função retornou o valor " + str(ret) + " quando nenhum valor de retorno era esperado")
        return ret
    try:    
        if len(signature(func).parameters)!=1:
            raise SolucaoInvalida("A função deve aceitar exatamente um parâmetro")
        for i in [0, 1, 2, 3, 4]:
            b = __NoWrite_ClasseExemplo_01(i)
            testa(b)
            if b.recupera_valor() == i and i != 0:
                raise SolucaoInvalida("A função não modificou o valor " + str(i))
            if b.recupera_valor() != 2*i:
                raise SolucaoInvalida("A função modificou o valor " + str(i) + " para " + str(b.recupera_valor()) + ". O valor esperado era + " + str(2*i))
    except SolucaoInvalida as ex:
        return SolucaoResultado(False, ex)
    return SolucaoResultado()

class __ex03_gabarito():
    numero=0
    # Na verdade este gabarito está INCORRETO
    def funcao_1():
        pass
    def funcao_2():
        pass

def valida_ex_03_01(cls):
    try:
        if type(cls)!=type:
            raise SolucaoInvalida(str(cls) + "não é uma classe válida")
        a = dir(cls)
        b = dir(__ex03_gabarito)
        for atr in b:
            if str(atr).startswith("_"):
                continue
            if atr not in a:
                raise SolucaoInvalida("A classe " + str(cls) + " não contém o elemento " + str(atr))
        if type(cls.numero)!=int:
            raise SolucaoInvalida("O atributo de classe numero não é int")
        if cls.numero!=0:
            raise SolucaoInvalida("O atributo de classe numero contém incialmente o valor "+str(cls.numero))
        if not ismethod(cls.funcao_1):
            if isfunction(cls.funcao_1):
              raise SolucaoInvalida("O elemento de classe funcao_1 é uma função! (Verifique se o decorador @classmethod está aplicado)")
            raise SolucaoInvalida("O elemento de classe funcao_1 não é um método de classe!")
        if cls.funcao_1.__self__ is None or cls.funcao_1.__self__ is not cls:
            SolucaoInvalida("O método de classe funcao_1 não está vinculado à classe " + str(cls))
        if len(signature(cls.funcao_1).parameters)!=0:
            raise SolucaoInvalida("O método de classe funcao_1 não deve aceitar parâmetros")
        if not ismethod(cls.funcao_2):
            if isfunction(cls.funcao_2):
              raise SolucaoInvalida("O elemento de classe funcao_2 é uma função! (Verifique se o decorador @classmethod está aplicado)")
            raise SolucaoInvalida("O elemento de classe funcao_2 não é um método de classe!")
        if cls.funcao_2.__self__ is None or cls.funcao_2.__self__ is not cls:
            SolucaoInvalida("O método de classe funcao_2 não está vinculado à classe " + str(cls))
        if len(signature(cls.funcao_2).parameters)!=0:
            raise SolucaoInvalida("O método de classe funcao_2 não deve aceitar parâmetros")
        try:
            ret = cls.funcao_1()
            if type(ret)!=int:
                raise SolucaoInvalida("A funcao_1 não retornou um inteiro")
            if ret != cls.numero:
                raise SolucaoInvalida("A funcao_1 não retornou o valor de numero")
            cls.numero+=1
            ret = cls.funcao_1()
            if type(ret)!=int:
                raise SolucaoInvalida("A funcao_1 não retornou um inteiro")
            if ret != cls.numero:
                raise SolucaoInvalida("A funcao_1 não retornou o valor de numero")
            cls.numero+=2
            ret = cls.funcao_1()
            if type(ret)!=int:
                raise SolucaoInvalida("A funcao_1 não retornou um inteiro")
            if ret != cls.numero:
                raise SolucaoInvalida("A funcao_1 não retornou o valor de numero")
            cls.funcao_2()
            if cls.numero!=0:
                raise SolucaoInvalida("A funcao_2 não resetou o valor de numero")
        except SolucaoInvalida as ex:
            raise ex
        except Exception as ex:
            raise SolucaoInvalida("O seu código lancou uma exceção") from ex
    except SolucaoInvalida as ex:
        return SolucaoResultado(False, ex)
    return SolucaoResultado()

class __ex031_gabarito():
    def __init__(self, v):
        pass
    def recupera_valor(self):
        pass
    
def valida_ex_03_02_01(cls):
    try:
        if type(cls)!=type:
            raise SolucaoInvalida(str(cls) + "não é uma classe válida")
        a = dir(cls)
        b = dir(__ex031_gabarito)
        for atr in b:
            if atr not in a:
                raise SolucaoInvalida("A classe " + str(cls) + " não contém o atributo " + str(atr))
        if type(cls.__init__)!=type(__ex031_gabarito.__init__):
            raise SolucaoInvalida("O atributo de classe __init__ não é uma função")
        if len(signature(cls.__init__).parameters)!=len(signature(__ex031_gabarito.__init__).parameters):
            raise SolucaoInvalida("A função __init__ deve receber "+ str(len(signature(__ex031_gabarito.__init__).parameters))+ " parâmetros")
        if type(cls.recupera_valor)!=type(__ex031_gabarito.recupera_valor):
            raise SolucaoInvalida("O atributo de classe recupera_valor não é uma função")
        if len(signature(cls.recupera_valor).parameters)!=len(signature(__ex031_gabarito.recupera_valor).parameters):
            raise SolucaoInvalida("A função recupera_valor deve receber "+ str(len(signature(__ex031_gabarito.recupera_valor).parameters))+ " parâmetro")
        try: 
            vals = [None, [0],[1],[1],[2],[3],[5],[8]]
            objs = []
            tst = __ex031_gabarito(None)
            for i in vals:
                o = cls(i)
                if type(o)!=cls:
                    raise SolucaoInvalida("O construtor não criou um objeto de instância.")
                objs.append(o)
            for i, o in enumerate(objs):
                if type(o.recupera_valor)!=type(tst.recupera_valor):
                    raise SolucaoInvalida("O campo recupera_valor do objeto " + str(o) + " não é um método")
                if len(signature(o.recupera_valor).parameters) > 0:
                    raise SolucaoInvalida("O método recupera_valor do objeto " + str(o) + " espera parâmetros")
                ret = o.recupera_valor()
                if ret is not vals[i]:
                    raise SolucaoInvalida("O método recupera_valor não retornou o objeto passado durante a inicialização.")
        except SolucaoInvalida as ex:
            raise ex
        except Exception as ex:
            raise SolucaoInvalida("O seu código lancou uma exceção") from ex
    except SolucaoInvalida as ex:
        return SolucaoResultado(False, ex)
    return SolucaoResultado()

class __ex0311_gabarito(__ex031_gabarito):
    def modifica_valor(self,v):
        pass
    
def valida_ex_03_03_01(cls, bcls=None):
    if bcls is not None:
        bres = valida_ex_03_02_01(bcls)
        if not bres:
            return bres
    try:
        if type(cls)!=type:
            raise SolucaoInvalida(str(cls) + "não é uma classe válida")
        if bcls is not None and cls.__bases__[0] is not bcls:
            raise SolucaoInvalida(str(cls) + "não é derivada diretamente de " + str(bcls))
        a = dir(cls)
        b = dir(__ex0311_gabarito)
        for atr in b:
            if atr not in a:
                raise SolucaoInvalida("A classe " + str(cls) + " não contém o atributo " + str(atr))
        if type(cls.__init__)!=type(__ex0311_gabarito.__init__):
            raise SolucaoInvalida("O atributo de classe __init__ não é uma função")
        if len(signature(cls.__init__).parameters)!=len(signature(__ex0311_gabarito.__init__).parameters):
            raise SolucaoInvalida("A função __init__ deve receber "+ str(len(signature(__ex0311_gabarito.__init__).parameters))+ " parâmetros")
        if type(cls.recupera_valor)!=type(__ex0311_gabarito.recupera_valor):
            raise SolucaoInvalida("O atributo de classe recupera_valor não é uma função")
        if len(signature(cls.recupera_valor).parameters)!=len(signature(__ex0311_gabarito.recupera_valor).parameters):
            raise SolucaoInvalida("A função recupera_valor deve receber "+ str(len(signature(__ex0311_gabarito.recupera_valor).parameters))+ " parâmetro")
        if type(cls.modifica_valor)!=type(__ex0311_gabarito.modifica_valor):
            raise SolucaoInvalida("O atributo de classe modifica_valor não é uma função")
        if len(signature(cls.modifica_valor).parameters)!=len(signature(__ex0311_gabarito.modifica_valor).parameters):
            raise SolucaoInvalida("A função modifica_valor deve receber "+ str(len(signature(__ex0311_gabarito.modifica_valor).parameters))+ " parâmetros")
        try: 
            vals = [None, (0),[1],(1),[2],[3],[5],[8]]
            vals2 = list(vals)
            vals2.reverse()
            objs = []
            tst = __ex0311_gabarito(None)
            for i in vals:
                o = cls(i)
                if type(o)!=cls:
                    raise SolucaoInvalida("O construtor não criou um objeto de instância.")
                objs.append(o)
            for i, o in enumerate(objs):
                if type(o.recupera_valor)!=type(tst.recupera_valor):
                    raise SolucaoInvalida("O campo recupera_valor do objeto " + str(o) + " não é um método")
                if len(signature(o.recupera_valor).parameters) > 0:
                    raise SolucaoInvalida("O método recupera_valor do objeto " + str(o) + " espera parâmetros")
                ret = o.recupera_valor()
                if ret is not vals[i]:
                    raise SolucaoInvalida("O método recupera_valor não retornou o objeto passado durante a inicialização.")
                ret = o.modifica_valor(vals2[i])
                if ret is not None:
                    raise SolucaoInvalida("O método modifica_valor não deveria retornar um valor.")
            for i, o in enumerate(objs):
                ret = o.recupera_valor()
                if ret is not vals2[i]:
                    raise SolucaoInvalida("O método recupera_valor não retornou o objeto passado durante a chamada a modifica_valor.")
        except SolucaoInvalida as ex:
            raise ex
        except Exception as ex:
            raise SolucaoInvalida("O seu código lancou uma exceção") from ex
    except SolucaoInvalida as ex:
        return SolucaoResultado(False, ex)
    return SolucaoResultado()

class __ex03113_gabarito(__ex0311_gabarito):
    def undo(self):
        pass
    
def valida_ex_03_04_01(cls, bcls):
    bres = valida_ex_03_03_01(bcls)
    if not bres:
        return bres
    try:
        if type(cls)!=type:
            raise SolucaoInvalida(str(cls) + "não é uma classe válida")
        if cls.__bases__[-1] is not bcls:
            raise SolucaoInvalida(str(cls) + "não é derivada diretamente de " + str(bcls))
        a = dir(cls)
        b = dir(__ex03113_gabarito)
        for atr in b:
            if atr not in a:
                raise SolucaoInvalida("A classe " + str(cls) + " não contém o atributo " + str(atr))
        if type(cls.__init__)!=type(__ex03113_gabarito.__init__):
            raise SolucaoInvalida("O atributo de classe __init__ não é uma função")
        if len(signature(cls.__init__).parameters)!=len(signature(__ex03113_gabarito.__init__).parameters):
            raise SolucaoInvalida("A função __init__ deve receber "+ str(len(signature(__ex03113_gabarito.__init__).parameters))+ " parâmetros")
        if type(cls.recupera_valor)!=type(__ex03113_gabarito.recupera_valor):
            raise SolucaoInvalida("O atributo de classe recupera_valor não é uma função")
        if len(signature(cls.recupera_valor).parameters)!=len(signature(__ex03113_gabarito.recupera_valor).parameters):
            raise SolucaoInvalida("A função recupera_valor deve receber "+ str(len(signature(__ex03113_gabarito.recupera_valor).parameters))+ " parâmetro")
        if type(cls.modifica_valor)!=type(__ex03113_gabarito.modifica_valor):
            raise SolucaoInvalida("O atributo de classe modifica_valor não é uma função")
        if len(signature(cls.modifica_valor).parameters)!=len(signature(__ex03113_gabarito.modifica_valor).parameters):
            raise SolucaoInvalida("A função modifica_valor deve receber "+ str(len(signature(__ex03113_gabarito.modifica_valor).parameters))+ " parâmetros")
        if type(cls.undo)!=type(__ex03113_gabarito.undo):
            raise SolucaoInvalida("O atributo de classe undo não é uma função")
        if len(signature(cls.undo).parameters)!=len(signature(__ex03113_gabarito.undo).parameters):
            raise SolucaoInvalida("A função undo deve receber "+ str(len(signature(__ex03113_gabarito.undo).parameters))+ " parâmetros")
        try: 
            vals = [None, (0),[1],(1),[2],[3],[5],[8]]
            vals2 = list(vals)
            vals2.reverse()
            objs = []
            tst = __ex03113_gabarito(None)
            for i in vals:
                o = cls(i)
                if type(o)!=cls:
                    raise SolucaoInvalida("O construtor não criou um objeto de instância.")
                objs.append(o)
            for i, o in enumerate(objs):
                if type(o.recupera_valor)!=type(tst.recupera_valor):
                    raise SolucaoInvalida("O campo recupera_valor do objeto " + str(o) + " não é um método")
                if len(signature(o.recupera_valor).parameters) > 0:
                    raise SolucaoInvalida("O método recupera_valor do objeto " + str(o) + " espera parâmetros")
                ret = o.recupera_valor()
                if ret is not vals[i]:
                    raise SolucaoInvalida("O método recupera_valor não retornou o objeto passado durante a inicialização.")
                ret = o.modifica_valor(vals2[i])
                if ret is not None:
                    raise SolucaoInvalida("O método modifica_valor não deveria retornar um valor.")
            for i, o in enumerate(objs):
                ret = o.recupera_valor()
                if ret is not vals2[i]:
                    raise SolucaoInvalida("O método recupera_valor não retornou o objeto passado durante a chamada a modifica_valor.")
                ret = o.undo()
                if ret is not None:
                    raise SolucaoInvalida("O método undo não deveria retornar um valor.")
                ret = o.recupera_valor()
                if ret is not vals[i]:
                    raise SolucaoInvalida("O método recupera_valor não retornou o objeto esperado depois de undo.")
        except SolucaoInvalida as ex:
            raise ex
        except Exception as ex:
            raise SolucaoInvalida("O seu código lancou uma exceção") from ex
    except SolucaoInvalida as ex:
        return SolucaoResultado(False, ex)
    return SolucaoResultado()


if __name__ != 'main':
    VerificaAmbiente()
